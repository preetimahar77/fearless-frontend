window.addEventListener('DOMContentLoaded', () => {
    const form = document.getElementById('login-form');
    form.addEventListener('submit', async event => {
        event.preventDefault();

        const data = Object.fromEntries(new FormData(form))
        const fetchOptions = {
        method: 'post',
        body: data,
        headers: {
            'Content-Type': 'application/json',
        }
        };
        const url = 'http://localhost:3000/login.html';
        const response = await fetch(url, fetchOptions);
        if (response.ok) {
        window.location.href = 'http://localhost:3000/login.html';
        } else {
        console.error(response);
        }
    });
    })
