function createCard(title, description, starts, ends, locationName, pictureUrl) {
    return `
        <div class="card">
        <img class="card-img-top" src="${pictureUrl}" alt="..."/>
        <div class="card-body">
        <div class="col-sm-12 col-md-6 col-lg-4 mb-4">
            <h5 class="card-title">${title}</h5>
            <h6 class="card-subtitle-mb2-text-muted">${locationName}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="list-group list-group-flush">
        <div class="container">
        <li class="list-group-item">Start Date: ${new Date(starts).toLocaleDateString()}</li>
        <li class="list-group-item">End Date: ${new Date(ends).toLocaleDateString()}</li>
        </div>
        </div>
        </div>
        </div>
    `;
    }
// function alert() {
//     return `<div class="alert alert-danger" role="alert">
//     <h4 class="alert-heading">Looks like thats a bad response></h4>
//     <p></p>
//     <p>Got nothing here for ya,sorry</p>
//     <hr>
//     <p class="mb-0">If you wanna keep looking at this error, you are more than welcome</p>
//     </div>
//     `;
//     }


    window.addEventListener('DOMContentLoaded', async () => {

        const url = 'http://localhost:8000/api/conferences/';

        try {
            const response = await fetch(url);

            if (!response.ok) {
            // Figure out what to do when the response is bad
            } else {
                const data = await response.json();

            for (let conference of data.conferences) {
                const detailUrl = `http://localhost:8000${conference.href}`;
                const detailResponse = await fetch(detailUrl);
                if (detailResponse.ok) {
                    const details = await detailResponse.json();
                    const title = details.conference.name;
                    const description = details.conference.description;
                    const purl = 'http://localhost:8000/api/locations/';
                    //const pictureUrl ='purl[0].${picture_url}';
                    const pictureUrl = details.conference.location.picture_url;
                    const starts = details.conference.starts;
                    const ends = details.conference.ends;
                    const locationName = details.conference.location.name;
                    const html = createCard(title, description, starts, ends, locationName, pictureUrl);
                    const cardContainer = document.querySelector('#card-container');
                    cardContainer.insertAdjacentHTML('beforeend', html);
                }
            }

            }
        } catch (e) {
            // Figure out what to do if an error is raised
        }

    });
