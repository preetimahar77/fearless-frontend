// Get the cookie out of the cookie store
const payloadCookie = await cookieStore.get('jwt_access_payload');
if (payloadCookie) {
    //console.log(payloadCookie.value)
  // The cookie value is a JSON-formatted string, so parse it
    const value = JSON.parse(payloadCookie.value);

  // Convert the encoded payload from base64 to normal string
    const decodedPayload = Buffer.from(payloadCookie.value, 'base64')// FINISH THIS

  // The payload is a JSON-formatted string, so parse it
    const payload = JSON.parse(decodedPayload)// FINISH THIS
    console.log(payloadCookie.value)
  // Print the payload
    console.log(payload);
    const permissions = payload.user.perms;
    const selectLocationTag = document.getElementById('nav-link1');
    const selectConferenceTag = document.getElementById('nav-link2');

  // Check if "events.add_conference" is in the permissions.
  // If it is, remove 'd-none' from the link
    if (permissions.includes("events.add_conference")) {
        selectConferenceTag.classList.remove('d-none');
    }


  // Check if "events.add_location" is in the permissions.
  // If it is, remove 'd-none' from the link
    if (permissions.includes("events.add_location")) {
        selectLocationTag.classList.remove('d-none');
}

}
