window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    const loadingTag = document.getElementById('loading-conference-spinner');
    const url = 'http://localhost:8000/api/conferences/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();

        for (let conference of data.conferences) {
            const option = document.createElement('option');
            option.value = conference.href;
            option.innerHTML = conference.name;
            selectTag.appendChild(option);
        }
        loadingTag.classList.add("d-none");
        selectTag.classList.remove("d-none");
        }
    else {
            return "cannot fetch data";
        }

    const formTag = document.getElementById('create-attendee-form');
    const successTag = document.getElementById('success-message');
    formTag.addEventListener('submit', async (event) => {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const AUrl = 'http://localhost:8001/api/attendees/new';
        const fetchConfig = {
            method : "post",
            body: json,
            headers: {'Content-type': 'application/json'},
        }
        console.log('Congratulations! You are all signed up!');
        const response = await fetch(AUrl, fetchConfig);
        if (response.ok ){
            formTag.reset();
            const newAttendee = await response.json();
            console.log(newAttendee);
        }
        })
    });


        // const response = await fetch(attendeeUrl, fetchConfig);
        // if (response.ok ){
        //     formTag.reset();
        //     //console.log('Congratulations! You are all signed up!');
        //     const newAttendee = await response.json();
        //     console.log(newAttendee);
        //     formTag.classList.add("d-none");
        //     successTag.classList.remove("d-none");
            // return `<div class="display" id="success-message">
            //     Congratulations! You're all signed up!
            //     </div>`;
       // }
    //})
