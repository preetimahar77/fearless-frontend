import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

function ConferenceForm(props) {

    const [name, setName] = useState('');
    const [starts, setStarts] = useState('');
    const [ends, setEnds] = useState('');
    const [description, setDescription] = useState('');
    const [max_presentations, setMax_presentations] = useState('');
    const [max_attendees, setMax_attendees] = useState('');
    const [location, setLocation] = useState('');
    const [locations, setLocations] = useState([]);

  //Notice that we can condense all formData
  //into one state object
    const [formData, setFormData] = useState('');
//     name: '',
//     starts: '',
//     ends: '',
//     description: '',
//     max_presentations: '',
//     max_attendees: '',
//     location: '',
//   })
    // const handleFormChange = (e) => {
    // const value = e.target.value;
    // const inputName = e.target.name;
    useEffect(() => {
        async function getLocations() {
          const url = 'http://localhost:8000/api/locations/';

          const response = await fetch(url);

          if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
          }
        }
        getLocations();
      }, []);

      async function handleSubmit(event) {
        event.preventDefault();
        const data = {
          name,
          starts,
          ends,
          description,
          location,
          max_presentations: max_presentations,
          max_attendees: max_attendees,
        };


        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newConference = await response.json();
          console.log(newConference);

          setName('');
          setStarts('');
          setEnds('');
          setDescription('');
          setMax_presentations('');
          setMax_attendees('');
          setLocation('');
        }
      }

        const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
        }
        const handleStartsChange = (event) => {
        const value = event.target.value;
        setStarts(value);
        }
        const handleEndsChange = (event) => {
        const value = event.target.value;
        setEnds(value);
        }
        const handleDescriptionChange = (event) => {
        const value = event.target.value;
        setDescription(value);
        }
        const handleMax_presentationsChange = (event) => {
        const value = event.target.value;
        setMax_presentations(value);
        }
        const handleMax_attendeesChange = (event) => {
        const value = event.target.value;
        setMax_attendees(value);
        }
        const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
        }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/';
        const response = await fetch(url);
        if (response.ok) {
        const data = await response.json();
        setLocations(data.locations);
        }
    }

    // useEffect(() => {
    //     fetchData();
    // }, []);

    // const handleSubmit = async (event) => {
    // event.preventDefault();
    // const data = {};
    // data.name = name;
    // data.starts = starts;
    // data.ends = ends;
    // data.description = description;
    // data.max_presentations = max_presentations;
    // data.max_attendees = max_attendees;
    // data.location = location;

    // const url = 'http://localhost:8000/api/conferences/';

    // const fetchConfig = {
    //     method: "post",
    //     //Because we are using one formData state object,
    //     //we can now pass it directly into our request!
    //     body: JSON.stringify(formData),
    //     headers: {
    //         'Content-Type': 'application/json',
    //     },
    //     };

    // const response = await fetch(url, fetchConfig);

    // if (response.ok) {
      //The single formData object
      //also allows for easier clearing of data
    //   setFormData({
    //     name: '',
    //     starts: '',
    //     ends: '',
    //     description: '',
    //     max_presentations: '',
    //     max_attendees: '',
    //     location: '',
    // const newResponse = await response.json();
    // console.log(newResponse);
    //     setName('');
    //     setStarts('');
    //     setEnds('');
    //     setDescription('');
    //     setMax_presentations('');
    //     setMax_attendees('');
    //     setLocation('');
    //     setLocations('');
    //     }
    // }


  //Notice that we can also replace multiple form change
  //eventlistener functions with one
    // const handleFormChange = (e) => {
    // const value = e.target.value;
    // const inputName = e.target.name;
        // const handlenameChange = (event) => {
        // const value = event.target.value;
        //     setName(value);
        // }
        // const handlestartsChange = (event) => {
        // const value = event.target.value;
        //     setStarts(value);
        // }
        // const handleendsChange = (event) => {
        // const value = event.target.value;
        //     setEnds(value);
        // }
        // const handledescriptionChange = (event) => {
        //     const value = event.target.value;
        //         setDescription(value);
        //     }
        // const handlemax_presentationsChange = (event) => {
        //     const value = event.target.value;
        //         setMax_presentations(value);
        //     }
        // const handlemax_attendeesChange = (event) => {
        //     const value = event.target.value;
        //         setMax_attendees(value);
        //     }
        // const handlelocationChange = (event) => {
        //     const value = event.target.value;
        //         setLocation(value);
        //     }

    //We can condense our form data event handling
    //into on function by using the input name to update it
    //setFormData({...formData,});
    // setFormData({
    //   //Previous form data is spread (i.e. copied) into our new state object
    //   ...formData,

    //   //On top of the that data, we add the currently engaged input key and value
    //   [inputName]: value
    // });
    // const formTag = document.getElementById('create-conference-form');
    // formTag.addEventListener('submit', async (event) => {
    //     event.preventDefault();
    //     const formData =  new FormData(formTag);
    //     const json = JSON.stringify(Object.fromEntries(formData));
    //     console.log(json);
    //     const conferenceUrl = 'http://localhost:8000/api/conferences/';
    //     //const conferenceId = selectTag.options[selectTag.selectedIndex].value;
    //     //const presentationUrl = `http://localhost:8000/api/conferences/${conferenceId}presentations/`;
    //     const fetchConfig = {
    //         method: "post",
    //         body: json,
    //         headers: {
    //             'Content-Type' : 'application/json',
    //         },
    //     };
    //     const response = await fetch(conferenceUrl, fetchConfig);
    //     if(response.ok) {
    //         formTag.reset();
    //         const newConference = await response.json();
    //         console.log(newConference);
    //     }
    //     });

    return (
    <div className="row">
        <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <Link to="conferences/new" className="btn btn-primary btn-lg px-4 gp-3"></Link>
            <form onSubmit={handleSubmit} id="create-conference-form">

            <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="Name" id="name" className="form-control" />
                <label htmlFor="name">Name</label>
            </div>

            <div className="form-floating mb-3">
                <input onChange={handleStartsChange} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control" />
                    <label htmlFor="starts">Starts</label>
            </div>

            <div className="form-floating mb-3">
                <input onChange={handleEndsChange} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control" />
                <label htmlFor="ends">Ends</label>
            </div>

            <div className="mb-3">
                <label htmlFor="description">Description</label>
                <input onChange={handleDescriptionChange} required type="text" id="description" rows="3" name="description" className="form-control" />
            </div>

            <div className="form-floating mb-3">
                <input onChange={handleMax_presentationsChange} placeholder="Maximum presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" />
                <label htmlFor="max_presentations">Maximum presentations</label>
            </div>

            <div className="form-floating mb-3">
                <input onChange={handleMax_attendeesChange} placeholder="Maximum attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" />
                <label htmlFor="max_attendees">Maximum attendees</label>
            </div>

            <div className="mb-3">
                <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                    <option value="">Choose a location</option>
                    {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>{location.name}</option>
                    );
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
        </div>
        </div>
    </div>
    );
    }

export default ConferenceForm;
