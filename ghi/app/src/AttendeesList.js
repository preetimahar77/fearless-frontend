import { React } from "react";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";
function AttendeesList() {

    const [attendees, setAttendees] = useState([]);
    const [href, setHref] = useState('');
    const [name, setName] = useState('');
    const [conference, setConference] = useState('');

    const handlehrefChange = (event) => {
    const value = event.target.value;
        setHref(value);
    }
    const handlenameChange = (event) => {
    const value = event.target.value;
        setName(value);
    }
    const handleConferenceChange = (event) => {
    const value = event.target.value;
        setConference(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        // create an empty JSON object
        const data = {};
        data.href = href;
        data.name = name;
        data.Conference = conference;
        console.log(data);

        const attendeeUrl = 'http://localhost:8001/api/conferences/conferenceid/attendees/';
        const fetchConfig = {
                method: "POST",
                body: JSON.stringify(data),
                headers: {
                "Content-Type": "application/json",
                },
            };
            const response = await fetch(attendeeUrl, fetchConfig);
            if (response.ok) {
                const newResponse = await response.json();
                console.log(newResponse);
                setHref('');
                setName('');
                setConference('');
                }
                }
        useEffect(() => {
            loadAttendeesList();
        }, []);

    async function loadAttendeesList() {
        for(var i = 1; i<= 6; i++) {
        const response = await fetch(`http://localhost:8001/api/conferences/${i}/attendees/`);
        if(response.ok) {
            const data = await response.json();
            setAttendees(data.attendees);
        }
        else {
            console.error(response);
        }
    }
    }
    return (
        <div>
        <br />
        <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
            <Link to="attendees/new" className="btn btn-primary btn-lg px-4 gp-3">Attendees List</Link>
        </div>
        <div className="form-floating mb-3">
        <br />
        <h2>Attendees List</h2>
        <table className="table table-success table-striped">
            <thead>
            <tr>
                <th>Name</th>
                <th>Conference</th>
            </tr>
            </thead>
            <tbody>
                {attendees.map(attendee => {return (<tr key={attendee.href}>
                    <td> {attendee.name}</td>
                    <td>{attendee.conference}</td>
                    </tr>);
                })}
            </tbody>
        </table>
        </div>
        </div>
        );
    }



export default AttendeesList;
