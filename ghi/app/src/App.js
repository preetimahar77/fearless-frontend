import './App.css';
import Nav2 from './Nav2';
import React from 'react';
import { ReactDOM } from 'react-dom';
import AttendeesList from './AttendeesList';
import './index.css';
import LocationForm from './LocationForm.js';
import AttendConferenceForm from './attendConferenceForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage.js';

function App(props) {
  if (props.attendees === undefined) {
    return null;
    }
  else {
  return (
    <BrowserRouter>
      <Nav2 />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="locations/new" element={<LocationForm />} />
          <Route path="attendees/new" element={<AttendeesList />} />
          <Route path="attendees" element={<AttendConferenceForm />} />
          <Route path="conferences/new" element={<ConferenceForm />} />
          <Route path="presentations" element={<PresentationForm />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
  }
}

export default App;
//<Route index element={<MainPage />} />
