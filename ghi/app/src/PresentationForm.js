import React, {useEffect, useState} from 'react';
import { Link } from 'react-router-dom';
function PresentationForm () {
    //const [location, setLocation] = useState('');
    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const [presenter_name, setPresenter_name] = useState('');
    const [presenter_email, setPresenter_email] = useState('');
    const [company_name, setCompany_name] = useState('');
    const [title, setTitle] = useState('');
    const [description, setDescription] = useState('');
    const [conference, setConference]= useState([]);

    const handlepresenter_nameChange = (event) => {
        const value = event.target.value;
        setPresenter_name(value);
        }
    const handlepresenter_emailChange = (event) => {
        const value = event.target.value;
            setPresenter_email(value);
            }
    const handlecompany_nameChange = (event) => {
    const value = event.target.value;
    setCompany_name(value);
    }
    const handletitleChange = (event) => {
    const value = event.target.value;
    setTitle(value);
    }
    const handledescriptionChange = (event) => {
    const value = event.target.value;
    setDescription(value);
    }
    const handleConferenceChange = (event) => {
    const value = event.target.value;
    setConference(value);
    }

    const [conferences, setConferences] = useState([]);

    const fetchData = async () =>{
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch (url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
            }
        }

    useEffect(() => {
        fetchData();
        }, []);

        const handleSubmit = async (event) => {
            event.preventDefault();
            // create an empty JSON object
            const data = {};
            data.presenter_name = presenter_name;
            data.presenter_email = presenter_email;
            data.company_name = company_name;
            data.title = title;
            data.description = description;
            data.conference = conference;
            console.log(data);


            const attendeeUrl = `http://localhost:8000/api/conferences/${conference.href}/presentations`;
            const fetchConfig = {
                    method: "post",
                    body: JSON.stringify(data),
                    headers: {
                    "Content-Type": "application/json",
                    },
                };
                const attendeeResponse = await fetch(attendeeUrl, fetchConfig, {mode: 'no-cors'});
                if (attendeeResponse.ok) {
                    setPresenter_name('');
                    setPresenter_email('');
                    setCompany_name('');
                    setTitle('');
                    setDescription('');
                    setConference('');
                    }
                    }
    return (
        <div className="container">
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <Link to="presentations" className="btn btn-primary btn-lg px-4 gp-3"></Link>
            <form onSubmit={handleSubmit} id="create-presentation-form">
            <div className="form-floating mb-3">
            <input onChange={handlepresenter_nameChange} placeholder="Presenter Name" required type="text" value={presenter_name} name="presenter_name" id="name" className="form-control"/>
            <label htmlFor="presenter_name">Presenter Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handlepresenter_emailChange} placeholder="Presenter email" required type="text" value={presenter_email} name="presenter_email" id="email" className="form-control"/>
                <label htmlFor="presenter_email">Presenter email</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handlecompany_nameChange} placeholder="Company Name" required type="text" name="company_name" value={company_name} id="name" className="form-control"/>
                <label htmlFor="company_name">Company Name</label>
            </div>
            <div className="form-floating mb-3">
                <input onChange={handletitleChange} placeholder="Title" required type="text" name="title" value={title} id="Title" className="form-control"/>
                <label htmlFor="Title">Title</label>
            </div>
            <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea onChange={handledescriptionChange} value={description} id="synopsis" rows="3" name="description" className="form-control"></textarea>
            </div>
            <div className="mb-3">
                <select required name="conference" id="conference" className="form-select" value={conference} onChange={handleConferenceChange}>
                    <option value="">Choose a conference</option>
                    {conferences.map(conference => {
                    return (<option key = {conference.href} value = {conference.href}>{conference.name}</option>);
                    })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
        </div>
        );
    }
    export default PresentationForm;
