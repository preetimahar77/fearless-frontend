import React from "react";
import { NavLink } from "react-router-dom";
function Nav() {
    return (
        <nav className="navbar navbar-expand lg navbar-light bg-light">
            <div className="container-fluid">
                <a className="navbar-brand" href="#">Conference go!</a>
                <div className="collapse Navbar-collapse" id="navbar supported content">
                    <ul className="navbar nav-me auto mb-2 mb-lg-0">
                        <React.Fragment>
                            <li>
                            <NavLink className="nav-link" aria-current="page" href="/">Home</NavLink>
                            </li>
                            <li>
                                <NavLink className="nav-link" aria-current="page" href="new-location.html">New Location</NavLink>
                            </li>
                            <li>
                                <NavLink className="nav-link" aria-current="page" href="http://localhost:3000/new-conference.html">New Conference</NavLink>
                            </li>
                            <li>
                                <NavLink className="nav-link" aria-current="page" href="http://localhost:3000/new-presentation.html">New Presentation</NavLink>
                            </li>
                        </React.Fragment>
                    </ul>
                </div>
                </div>
        </nav>
    );
    }

    export default Nav;
