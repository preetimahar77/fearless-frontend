import React, {useEffect, useState} from 'react';
import { Link } from "react-router-dom";
function LocationForm() {
    //const [location, setLocation] = useState('');
    const [states, setStates]= useState([]);  // new code
    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [state, setState] = useState('');

    const handleRoomCountChange = (event) => {
    const value = event.target.value;
    setRoomCount(value);
    }
    const handleNameChange = (event) => {
    const value = event.target.value;
    setName(value);
    }
    const handleCityChange = (event) => {
    const value = event.target.value;
    setCity(value);
    }
    const handleStateChange = (event) => {
    const value = event.target.value;
    setState(value);
    }
    const handleSubmit = async (event) => {
        event.preventDefault();
        // create an empty JSON object
        const data = {};
        data.room_count = roomCount;
        data.name = name;
        data.city = city;
        data.state = state;
        console.log(data.state);

    const locationUrl = 'http://localhost:8000/api/locations/';
    const fetchConfig = {
            method: "POST",
            body: JSON.stringify(data),
            headers: {
            "Content-Type": "application/json",
            },
        };
    const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
        const newLocation = await response.json();
        console.log(newLocation);
        setName('');
        setRoomCount('');
        setCity('');
        setState('');
        }
    }
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/';
        const response = await fetch(url);
        if(response.ok) {
            const data = await response.json();
            setStates(data.states);
            const selectTag = document.getElementById('state');
            for (let state of data.states) {
                const option = document.createElement('option');
                option.value = state.abbreviation;
                option.innerHTML = state.name;
                selectTag.appendChild(option);
            }
        }
        else {
            console.error(response);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

        // async function loadLocationForm()  {
        //     const url = 'http://localhost:8000/api/states/';
        //     const response = await fetch(url);
        //     if(response.ok) {
        //         const data = await response.json();
        //         setStates(data.states);
        //         const selectTag = document.getElementById('state');
        //         for (let state of data.states) {
        //             const option = document.createElement('option');
        //             option.value = state.abbreviation;
        //             option.innerHTML = state.name;
        //             selectTag.appendChild(option);
        //         }
        //     }
        //     else {
        //         console.error(response);
        //     }
        // }

    return (
        <div className="container">
        <div className="row">
            <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h3>Create a new location</h3>
            <Link to="locations/new" className="btn btn-primary btn-lg px-4 gp-3"></Link>
            <div className="d-grid gap-5 d-sm-flex justify-content-sm-center">
            <form onSubmit={handleSubmit} id="create-location-form">
                <div className="form-floating mb-3">
                <input onChange={handleNameChange} placeholder="Name" required type="text" name="name" value={name} id="name" className="form-control"/>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleRoomCountChange} placeholder="Room count" required type="number" name="room_count" value={roomCount} id="room_count" className="form-control"/>
                <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                <input onChange={handleCityChange} placeholder="City" required type="text" name="city" id="city" value={city} className="form-control"/>
                <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                <select onChange={handleStateChange} required name="state" id="state" value={state} className="form-select">
                    <option value="">Choose a state</option>
                    {states.map(state => {
                    return (
                    <option key ={state.abbreviation} value={state.abbreviation}>{state.name}</option>
                    );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
                </form>
                </div>
            </div>
            </div>
        </div>
        </div>
    );
    }
    export default LocationForm;
